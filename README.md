# Vorlage für impress.js-Präsentation
- impress.txt Beispiel-Präsentation
- style.css CSS-Stylesheet
- impress-template.html Template mit Titelfolie aus Header-Angaben
- images-Ordner mit Uni-Logo
- make.sh Make-File für impress.js-Präsentation, LaTeX-Beamer-Präsentation und LaTeX-Handout