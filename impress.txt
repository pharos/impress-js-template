% Titel
% Autor
% Datum


# Erste Folie {.step data-x=-2000 data-y=0 data-scale=1}
Hier ist ein wenig Text

# Worum geht es? {.step data-x=0 data-y=2000 data-scale=3}

Ein paar Punkte:

* Erster Punkt
* Zweiter Punkt
    * ein Unterpunkt
    * noch einer
* Dritter Punkt

# Noch eine Folie {.step data-x=2000 data-y=0 data-z=1000 data-scale=0.5}

Weiterer Text

# Noch eine {.step data-x=0 data-y=-2000}

Noch mehr Text

# Und noch eine... {.step data-x=2000 data-y=-2000}

Immer noch Text


# Und noch eine {.step data-x=4000 data-y=-2000}

Weiterer Text

# Letzte Folie {.step data-x=6000 data-y=-2000}

Das war's

# {#overview .step data-scale=10}